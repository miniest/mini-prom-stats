/* eslint-env mocha */

import { Stats } from "../../Stats.js";
import { Counter } from "../../Counter.js";
import chai from "chai";

const assert = chai.assert;

const result1 = `# HELP requests_total Total Requests
# TYPE requests_total counter
requests_total{method="GET",statusCode="200"} 1
# HELP samples_total Total samples
# TYPE samples_total counter
samples_total{method="GET",statusCode="200"} 1
`;

describe("Stats", async () => {
    it("toProm()", async () => {
        const stats = new Stats();

        stats.addMetric(new Counter("requests_total", "Total Requests"));
        stats.addMetric(new Counter("samples_total", "Total samples"));

        stats
            .getMetric("requests_total")
            .withLabels({ statusCode: "200", method: "GET" })
            .inc();
        stats
            .getMetric("samples_total")
            .withLabels({ statusCode: "200", method: "GET" })
            .inc();

        assert.equal(stats.toProm(), result1);
    });
});
