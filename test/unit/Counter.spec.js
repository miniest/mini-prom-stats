/* eslint-env mocha */

import chai from "chai";
import { Counter } from "../../Counter.js";

const assert = chai.assert;

describe("Counter", async () => {
    it("withLabels()", async () => {
        const counter = new Counter("test_counter");

        const inc1 = counter.withLabels({ lable1: "aa", label2: "bbb" });
        const inc2 = counter.withLabels({ lable1: "aa", label2: "bbb" });
        const inc3 = counter.withLabels({ lable1: "aa", label2: "ccc" });
        const inc4 = counter.withLabels({ lable3: "aa", label2: "bbb" });
        const inc5 = counter.withLabels({ lable2: "bbb", label1: "aa" });

        assert.strictEqual(
            inc1,
            inc2,
            "incrementors for same labels should be same"
        );
        assert.notStrictEqual(
            inc1,
            inc3,
            "incrementors for different labels should be different"
        );
        assert.strictEqual(
            inc1,
            inc2,
            "incrementors for same labels even though in different order should be same"
        );
        assert.notStrictEqual(
            inc4,
            inc5,
            "incrementors with different labels should be different"
        );
    });

    it("toProm()", async () => {
        const counter = new Counter("Test_counter", "test_help");

        counter.withLabels({ label1: "aa" }).inc();
        counter.withLabels({ label1: "bb", label2: "ccc" }).inc();

        assert.equal(
            counter.toProm(),
            `# HELP Test_counter test_help
# TYPE Test_counter counter
Test_counter{label1="aa"} 1
Test_counter{label1="bb",label2="ccc"} 1
`
        );
    });
});
