/**
 * Represents group of metrics that can be represented as consolidated view.
 */
export class Stats {
    constructor() {
        this._metrics = new Map();
    }

    /**
     * Add metric to list of metrics
     *
     * @param {Metric} metric - metric to add
     */
    addMetric(metric) {
        this._metrics.set(metric.name, metric);
    }

    /**
     * Get metric by name
     *
     * @param {string} name - name of the metric
     *
     * @return {Metric}
     */
    getMetric(name) {
        return this._metrics.get(name);
    }

    /**
     * Get representation of the stats aggregated view in the prometheus format
     *
     * @return {string}
     */
    toProm() {
        return Array.from(this._metrics.values()).reduce(
            (acc, v) => acc + v.toProm(),
            ""
        );
    }
}
