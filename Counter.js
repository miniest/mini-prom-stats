import _ from "lodash";

/**
 * Representation of value assigned to labels combination and counter.
 */
class Incrementer {
    /**
     * Base constructor
     *
     * @param {object} lables reference to the labels object this refference is associated with
     * @param {Counter} counter instance of parent counter
     * @param {number} [initValue=0] intial counter value
     */
    constructor(labels, counter, initVal = 0) {
        this._labels = labels;
        this._parent = counter;
        this._val = initVal;
        this._labels = labels;
    }

    /**
     * Increment value of counter
     *
     * @param {number} amount how much to increase filter
     */
    inc(amount = 1) {
        this._val += amount;
    }

    /**
     * Returns string representation of incrementer.
     *
     * @return {string}
     */
    toString() {
        return `${this._val}`;
    }
}

/**
 * Counter type representation.
 */
export class Counter {
    /**
     * Constructor
     *
     * @param {string} name        - name of metric
     * @param {string} [help=""]   - description of metric
     * @param {object} [labels={}] - common labels for counter
     */
    constructor(name, help = "", labels = {}) {
        // TODO name is required
        // TODO samitize name (replace anything not [a-zA-Z0-9_])

        this._name = name;
        this._help = help;
        this._val = 0;
        this._labels = labels;
        this._incrementers = new Map();
    }

    /**
     * Name of the counter.
     *
     * @type {string}
     * @readonly
     *
     */
    get name() {
        return this._name;
    }

    /**
     * Increment value of the counter
     *
     * @param {number} [amount=1] - how much to increase the counter
     */
    inc(amount = 1) {
        this.withLabels(this._labels).inc(amount);
    }

    /**
     * Get incrementer for particular lables. Labels are merged with counter level labels.
     *
     * @param {object} [lables={}] - additional labels
     *
     * @return {Incrementer}
     */
    withLabels(labels = {}) {
        // TODO sanitize label names
        let newLabels = _.cloneDeep(this._labels);
        newLabels = _.merge(newLabels, labels);

        let hash = Object.entries(newLabels)
            .map(v => {
                return `${v[0]}="${v[1]}"`;
            })
            .sort()
            .join(",");
        hash = `{${hash}}`;

        if (this._incrementers.has(hash)) {
            return this._incrementers.get(hash);
        } else {
            const incrementer = new Incrementer(newLabels, this);
            this._incrementers.set(hash, incrementer);
            return incrementer;
        }
    }

    /**
     * Get representation of the counter in the prometheus format
     *
     * @return {string}
     */
    toProm() {
        if (this._incrementers.size > 0) {
            return Array.from(this._incrementers.entries()).reduce(
                (acc, v) => acc + `${this._name}${v[0]} ${v[1]}\n`,
                `# HELP ${this._name} ${this._help}\n# TYPE ${this._name} counter\n`
            );
        } else {
            return "";
        }
    }
}
